@extends('layout.default')

{{-- Styles Section --}}
@section('styles')
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
@endsection

{{-- Content --}}
@section('content')
    @foreach($data as $i)
        {{--    Breadcrumbs     --}}
        <div class="row mb-5">
            <div class="col">
                <div class="card card-custom py-0 px-5 ">
                    <div class="card-header border-0 pl-1 py-5" style="min-height: auto">
                        <div class="card-title m-0">
                            <h3 class="card-label">Products :: <a href="{{ route('products.optimization', $i->id) }}">{{ $i->brand_name }}</a></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--   Card Description     --}}
        <div class="row">
            <div class="col">
                <input id="product_id" type="hidden" name="product_id" value="{{ $i->id }}">
                <input id="csrf-token" name="csrf-token" type="hidden" value="{{ csrf_token() }}" />
                <div class="card p-4 border-radius-10">
                    <div class="card-body p-3">
                        <div class="row">
                            <div class="col-8">
                                <div class="row justify-content-between my-2">
                                    <div class="col">
                                        <p class="h2">{{$i->brand_name}}</p>
                                    </div>
                                    <div class="col">
                                        <p class="h2">{{$i->mpn}}</p>
                                    </div>
                                    <div class="col">
                                        <p class="h2">{{$i->bin_picking_number}}</p>
                                    </div>
                                </div>
                                <div class="row justify-content-between my-2">
                                    <div class="col">
                                        <p class="h4">{{$i->sku}}</p>
                                    </div>
                                    <div class="col">
                                        <p class="h4">Sales Rank: 101,253</p>
                                    </div>
                                    <div class="col">
                                        <p class="h4">Velocity: 1/23 Days</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="row justify-content-center my-2 align-items-center">
                                    <div class="col-6 text-center">
                                        <button
                                            type="button"
                                            id="picker"
                                            class="btn btn-sm btn-block border dropdown-toggle form-control form-control-sm border-radius-10">
                                            Date Range
                                        </button>
                                    </div>
                                    <div class="col-3 text-center">
                                        <a href="{{ route('products.alarm.edit', $i->id) }}">
                                            <svg width="1.6em" height="1.6em"
                                                 data-toggle="popover" data-content="Alarm"
                                                 data-placement="top"
                                                 viewBox="0 0 16 16" class="bi bi-alarm" fill="grey"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" d="M8 15A6 6 0 1 0 8 3a6 6 0 0 0 0 12zm0 1A7 7 0 1 0 8 2a7 7 0 0 0 0 14z"/>
                                                <path fill-rule="evenodd" d="M8 4.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.053.224l-1.5 3a.5.5 0 1 1-.894-.448L7.5 8.882V5a.5.5 0 0 1 .5-.5z"/>
                                                <path d="M.86 5.387A2.5 2.5 0 1 1 4.387 1.86 8.035 8.035 0 0 0 .86 5.387zM11.613 1.86a2.5 2.5 0 1 1 3.527 3.527 8.035 8.035 0 0 0-3.527-3.527z"/>
                                                <path fill-rule="evenodd" d="M11.646 14.146a.5.5 0 0 1 .708 0l1 1a.5.5 0 0 1-.708.708l-1-1a.5.5 0 0 1 0-.708zm-7.292 0a.5.5 0 0 0-.708 0l-1 1a.5.5 0 0 0 .708.708l1-1a.5.5 0 0 0 0-.708zM5.5.5A.5.5 0 0 1 6 0h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                                                <path d="M7 1h2v2H7V1z"/>
                                            </svg>
                                        </a>
                                    </div>
                                    <div class="col-3 text-center">
                                        <a href="{{ route('products.notifications.list', $i->id) }}">
                                            <i class="fas fa-bell font-size-15 text-danger"
                                               data-toggle="popover"
                                               data-content="Notification"
                                               data-placement="top"
                                               aria-hidden="true"
                                            ></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="row my-2">
                                    <div class="col-3 text-center">
                                        <a href="{{ route('products.searchlinks.main', [$i->id])}}"
                                           data-toggle="popover"
                                           data-content="Search Links"
                                           data-placement="top">
                                            <img width="25rem" src="{{ asset('/images/icons/optimization/search.png')}}" alt="">
                                        </a>
                                    </div>
                                    <div class="col-3 text-center">
                                        <i class="fas fa-gift text-warning mr-2"></i>
                                    </div>
                                    <div class="col-3 text-center">
                                        <i class="fas fa-truck-moving text-dark"></i>
                                    </div>
                                    <div class="col-3 text-center">
                                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/amazon-prime.png')}}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <section id="detailsChart">
            <div class="row mt-5">
                <div class="col-7">
                    <div class="card border-radius-10 customChartCard">
                        <div class="card-body">
                            <div class="row mb-2" >
                                <div class="col greyGradient border-radius-10 p-3" >
                                    <canvas id="salesChart" width="400" height="100"></canvas>
                                </div>
                            </div>
                            <div class="row mb-2" >
                                <div class="col greyGradient border-radius-10 p-3">
                                    <canvas id="buyBox" width="400" height="100" ></canvas>
                                </div>
                            </div>
                            <div class="row" >
                                <div class="col d-flex flex-column justify-content-center align-items-center">
                                    <div class="w-75">
                                        <div class="progress" style="height: 20px">
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                            <div class="progress-bar bg-danger" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                            <div class="progress-bar bg-success" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                            <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div>
                                    <div class="row w-75">
                                        <div class="col">From: <span id="fromRange"> Oct</span> </div>
                                        <div class="col text-right">To: <span id="toRange"> Sept</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-5 d-flex flex-column justify-content-between rightSideDataCol">
                    <div class="card dataCard border-radius-10">
                        <div class="card-body px-3 py-1">
                            <table class="table table-borderless table-sm">
                                <thead class="">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Current</th>
                                        <th scope="col">Lowest</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Price</th>
                                        <td class="font-weight-bold">19.99</td>
                                        <td class="font-weight-bold">18.73</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Shipping</th>
                                        <td class="font-weight-bold">4.63</td>
                                        <td class="font-weight-bold">-</td>
                                    </tr>
                                    <tr class="border-top">
                                        <th scope="row">Total</th>
                                        <td class="font-weight-bold">23.45</td>
                                        <td class="font-weight-bold">19.99</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card futureCard border-radius-10  d-flex align-items-center justify-content-center">
                        <h1 class="text-question text-center">Future use</h1>
                    </div>
                </div>
            </div>
        </section>
        <section id="asinPerformance">
            <div class="row mt-5">
                <div class="col">
                    <div class="card border-radius-10 py-5" style="height: 300px">
                        {{-- buttons--}}
                        <div class="row justify-content-center">
                            <div class="col-11">
                                <div class="row my-5">
                                    <div class="col d-flex justify-content-center">
                                        <a role="button" class="btn btn-block btnAsin border-radius-10 font-weight-boldest active" data-text="actual">
                                            <span class="font-weight-bold">Current ASIN Performance Data</span>
                                        </a>
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <a role="button" class="btn btn-block btnAsin border-radius-10 font-weight-boldest" data-text="alternate">
                                            <span class="font-weight-bold">Alternate ASIN Data</span>
                                        </a>
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <a role="button" class="btn btn-block btnAsin border-radius-10 font-weight-boldest" data-text="history">
                                            <span class="font-weight-bold"> History</span>
                                        </a>
                                    </div>
                                </div>
                                {{--Data--}}
                                <div class="row my-5">
                                    <div class="col">
                                        <table class="table table-bordered border-radius-10" style="border-collapse: unset">
                                            <thead class="text-center border-radius-10">
                                                <tr>
                                                    <th>Offers</th>
                                                    <th>Google Serp</th>
                                                    <th>AMZN Position</th>
                                                    <th>Low Price</th>
                                                    <th>Sugg. Low Price</th>
                                                    <th>Feedback %</th>
                                                    <th>Feedback #</th>
                                                    <th>BB Price</th>
                                                    <th>Impressions</th>
                                                    <th>CTR</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableData">
                                                <tr>
                                                    <td>6</td>
                                                    <td>3</td>
                                                    <td>2</td>
                                                    <td>$18.90</td>
                                                    <td>$18.29</td>
                                                    <td>82%</td>
                                                    <td>12</td>
                                                    <td>18.49</td>
                                                    <td>1.2k</td>
                                                    <td>4.6%</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endforeach
@endsection

@section('scripts')
    <script src="/js/customJs/details_amazon/chart.js"></script>
    <script src="/js/customJs/details_amazon/index.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>

@endsection
