{{-- Extends layout --}}
@extends('layout.default')
{{-- Styles Section --}}
@section('styles')
    
@endsection

{{-- Content --}}
@section('content')
<!-- new blade -->

<div id="product" class="row mb-2 pl-5 pr-4 justify-content-center">
    <div class="card card-custom col-12 col-md-12 col-lg-12 title bg-white w-100 py-3 px-5 d-flex justify-content-between">
        <div class="card-header pl-1 pt-0 flex-wrap border-0 pb-0 d-flex justify-content-between" style="min-height: auto;">
            <div class="card-title">
                <h3 class="card-label mb-0"><a href="{{ route('products.list') }}">Products</a> :: Optimization</h3>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12 d-flex flex-column">
        @if ($message = session('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ $message }}
            </div>
        @endif

        @if ($error = session('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ $error }}
            </div>
        @endif

        @if ($warning = session('warning'))
            <div class="alert alert-warning alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ $warning }}
            </div>
        @endif
    </div>
</div>
{{-- Optimization --}}
@foreach ($data as $i)
<div class="row justify-content-center justify-content-md-between justify-content-lg-between mt-5">
    @if($i->child_product_id === null)
        <input id="id" type="hidden" name="id" value="{{ $i->id }}">
        @else
        <input id="id" type="hidden" name="id" value="{{ $i->primary_product_id }}">
    @endif
    <input name="csrf-token" type="hidden" value="{{ csrf_token() }}" />
    <div class="col-12 col-sm-12 col-md-2 col-lg-2 mb-5 mb-md-0 mb-lg-0 ">
        <div class="bg-white cardProduct h-100 w-100 d-flex justify-content-center align-items-center py-5">
            @if(isset($i->product_id) )
                    <img
                         class="image zoom cursor-pointer"
                         data-toggle="modal" data-target="#exampleModal"
                         @if($i->url_thumbnail != "")
                             src="{{ $i->url_thumbnail }}"
                         @elseif($i->url_thumbnail2 != "")
                             src="{{ $i->url_thumbnail2 }}"
                         @endif
                    >
                @else
                    <div class="con">
                        <img data-toggle="modal" data-target="#exampleModal"
                             class="image zoom cursor-pointer"
                             @if($i->url_thumbnail != "")
                                 src="{{ $i->url_thumbnail }}"
                             @elseif($i->url_thumbnail2 != "")
                                 src="{{ $i->url_thumbnail2 }}"
                             @endif
                        >
                        <span class="text-white new-item top-right">new item</span>
                    </div>
            @endif
        </div>
        {{--   modal     --}}
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                    <p class="h6 text-center"> Mouse over to zoom the image</p>
                </div>
                    <div class="modal-body d-flex justify-content-center">
                        <span class='zoom cursor-pointer' id='ex1'>
                            <img id="zoomImage"
                                 @if($i->url_zoom != "")
                                     src="{{ $i->url_zoom }}"
                                 @elseif($i->url_zoom2 != "")
                                     src="{{ $i->url_zoom2 }}"
                                 @endif
                            />`
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Product Card  --}}
    <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
        <div class="bg-white cardProduct py-5 px-3">
            <div class="row  w-100 flex-column flex-sm-column flex-md-column flex-lg-column flex-xl-row">
                <div class="col-12 col-md-12 col-lg-12 col-xl-8 d-flex flex-column justify-content-between align-items-around pl-5 ">
                    <!-- first row -->
                    <div class="row w-100 d-flex flex-column flex-md-row flex-lg-row justify-content-between align-items-center align-items-md-center align-items-lg-center px-3">
                        {{-- Brand --}}
                        <div class="col-12 col-md-4 col-lg-4 d-flex justify-content-start align-items-center">
                            <h1 class="mb-0 mr-3">{{ $i->brand_name }}</h1>
                            @if($i->child_product_id != null)
                                <a href="{{ route('products.optimization', $i->primary_product_id) }}">
                                    <i class="fas fa-external-link-alt text-primary"
                                       data-toggle="popover"
                                       data-content="go Parent"
                                       data-placement="top"></i>
                                </a>
                            @endif
                        </div>
                        {{-- Model --}}
                        <div class="col-12 col-md-4 col-lg-4 d-flex flex-row justify-content-start align-items-center">
                            <h3 class="mb-0 mr-3">{{ $i->mpn }}</h3>
                            @if($i->child_product_id != null)
                                <i class="cursor-pointer fas fa-clone text-warning"
                                   data-toggle="popover"
                                   data-content="Cloned"
                                   data-placement="top"></i>
                                @else
                                <i class="text-success cursor-pointer font-size-15 far fa-check-circle"
                                   data-toggle="popover"
                                   data-content="Original Product"
                                   data-placement="top"
                                ></i>
                            @endif
                        </div>
                        {{-- SKU --}}
                        <div class="col-12 col-md-4 col-lg-4">
                            <h3 class="text-left text-md-right text-lg-right">{{ $i->sku }}</h3>
                        </div>
                    </div>
                    <!-- second row -->
                    <div class="row w-100 justify-content-between align-items-center px-3">
                        <div class="col-11 col-md-3 col-lg-3"
                             data-toggle="popover"
                             data-content="Rating"
                             data-placement="top">
                            <i data-star="1" class="fas fa-star cursor-pointer
                                @if($i->star_rating >= '1')
                                    starYellow
                                @endif
                            "></i>
                            <i data-star="2" class="fas fa-star cursor-pointer
                                @if($i->star_rating >= '2')
                                    starYellow
                                @endif
                            "></i>
                            <i data-star="3" class="fas fa-star cursor-pointer
                                @if($i->star_rating >= '3')
                                    starYellow
                                @endif
                            "></i>
                            <i data-star="4" class="fas fa-star cursor-pointer
                                @if($i->star_rating >= '4')
                                    starYellow
                                @endif
                            "></i>
                            <i data-star="5" class="fas fa-star cursor-pointer
                                @if($i->star_rating ==  '5')
                                    starYellow
                                @endif
                            "></i>
                        </div>
                        <div class="col-12 col-md-9 col-lg-9 d-flex flex-column flex-md-row flexlg-row justify-content-between w-100">
                            <div class="d-flex flex-row position-relative align-items-baseline">
                                <h5>Bin: &nbsp</h5>
                                <div id="bin" class="text-primary cursor-pointer">
                                    {{$i->bin_picking_number ?? 'Not set'}}
                                </div>
                                <input id="binInput" class="form-control-sm" type="text" value="" placeholder="Change Bin">
                            </div>
                            <div class="d-flex flex-row position-relative align-items-baseline">
                                <h5>QOH: &nbsp</h5>
                                <div id="qoh" class="text-primary cursor-pointer">
                                    {{$i->inventory_level ?? 'Not set'}}
                                </div>
                                <input id="qohInput" class="form-control-sm" type="number" value="" placeholder="Change Inventory level">
                            </div>
                            <h5 class="text-left text-md-right text-lg-right">
                                DLO: <span>  &nbsp{{ \Carbon\Carbon::parse($i->last_optimized)->format('m/d/y') }}</span>
                            </h5>
                        </div>
                    </div>
                    <!-- third row -->
                    <div class="row px-3">
                        <div class="col d-flex flex-row">
                            <h6 class="">Location: &nbsp</h6>
                            <div id="location" class="text-primary cursor-pointer" >
                                @if(isset($i->location))
                                    {{$i->location}}
                                @else
                                    TBD
                                @endif
                            </div>
                            <input id="locationInput" class="form-control-sm p-2" type="text" placeholder="Enter location" value="" >
                        </div>
                    </div>
                    <!-- 4th row radio buttons -->
                    <div class="row px-3 d-flex flex-column flex-md-row flex-lg-row align-items-center justify-content-center">
                        <!-- radio buttons -->
                        <div class="col-12 col-xl-6 mb-2 pt-2 d-flex justify-content-between align-items-baseline">
                            <div id="ajax_classification" class="">
                                <div class="btn-group btn-block btn-group-toggle" data-toggle="buttons">
                                    <label
                                        id="classificationLabel1"
                                        style="z-index: 3;" class="mr-n4  btn btn-secondary borderLeftRadius btn-sm rounded-pill
                                        @if(isset($i->classification))
                                            @if($i->classification == "rare")
                                                active
                                            @else
                                                btn-secondary
                                            @endif
                                        @else
                                            btn-secondary
                                        @endif
                                            ">
                                            <input type="radio"
                                            class="btn-sm"
                                            name="classification"
                                            id="classification_rare"
                                            value="rare"
                                            onchange="ajax_update('/products/optimization/ajax/classification')"
                                            @if(isset($i->classification))
                                                @if($i->classification == "rare")
                                                    checked
                                                @endif
                                            @endif
                                            >
                                                Rare
                                    </label>
                                    <label
                                    id="classificationLabel2"
                                    class="pl-5 btn-sm btn btn-secondary borderRightRadius
                                        @if(isset($i->classification))
                                            @if($i->classification == "common")
                                                active
                                            @else
                                                btn-secondary
                                            @endif
                                        @else
                                            btn-secondary
                                        @endif
                                        ">
                                        <input type="radio"
                                            class="btn-sm"
                                            onchange="ajax_update('/products/optimization/ajax/classification')"
                                            name="classification"
                                            id="classification_common"
                                            value="common"
                                            @if(isset($i->classification))
                                                @if($i->classification == "common")
                                                    checked
                                                @endif
                                            @endif
                                            > Common
                                    </label>
                                </div>
                            </div>
{{--                            {{dd($market->status)}}--}}
                            @if ($i->mpn != "")
                            <input type="hidden" id="mpn" name="mpn" value="{{ $i->mpn }}">
                                @if($i->child_product_id === null)
                                    <input id="id" type="hidden" name="id" value="{{ $i->id }}">
                                @else
                                    <input id="id" type="hidden" name="id" value="{{ $i->primary_product_id }}">
                                @endif
                            <div id="ajax_market" class="text-right">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label
                                    class="btn btn-secondary borderLeftRadius btn-sm
                                        @if(!empty($market))
                                            @if($market->status == "Never")
                                                active
                                            @else
                                                btn-secondary
                                            @endif
                                        @else
                                            btn-secondary
                                        @endif
                                        ">
                                        <input type="radio"
                                        onchange="ajax_update('/products/optimization/ajax/market')"
                                                class="btn-sm"
                                                name="market_status"
                                                value="Never"
                                                id="market1"
                                                autocomplete="off"
                                                @if(isset($market->status))
                                                    @if($market->status == "Never")
                                                        checked
                                                    @endif
                                                @endif
                                        > Never
                                    </label>
                                    <label
                                    {{--  name_attr="market_status"--}}
                                    class="btn btn-secondary btn-sm
                                        @if(!empty($market))
                                            @if($market->status == "Add")
                                                active
                                            @else
                                                btn-secondary
                                            @endif
                                        @else
                                            btn-secondary
                                        @endif
                                        ">
                                        <input type="radio"
                                                class="btn-sm"
                                                name="market_status"
                                                value="Add"
                                                onchange="ajax_update('/products/optimization/ajax/market')"
                                                id="market2"
                                                autocomplete="off"
                                                @if(isset($market->status))
                                                    @if($market->status == "Add")
                                                        checked
                                                    @endif
                                                @endif
                                        > Add
                                    </label>
                                    <label class="btn btn-secondary borderRightRadius btn-sm
                                        @if(!empty($market))
                                            @if($market->status == "Gem")
                                                active
                                            @else
                                                btn-secondary
                                            @endif
                                        @else
                                            btn-secondary
                                        @endif
                                        ">
                                        <input type="radio"
                                                class="btn-sm"
                                                name="market_status"
                                                value="Gem"
                                                id="market3"
                                                onchange="ajax_update('/products/optimization/ajax/market')"
                                                autocomplete="off"
                                                @if(isset($market->status))
                                                    @if($market->status == "Gem")
                                                        checked
                                                    @endif
                                                @endif
                                        > Gem
                                    </label>
                                </div>
                            </div>
                            @else
                                {{-- mpn is not set disabled --}}
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary borderLeftRadius btn-sm">
                                        <input type="radio"
                                               onchange="ajax_update('/products/optimization/ajax/market')"
                                               class="btn-sm"
                                               name="market_status"
                                               value="Never"
                                               id="market1"
                                               autocomplete="off"
                                                > Never
                                    </label>
                                    <label class="btn btn-success btn-sm active">
                                        <input type="radio"
                                               onchange="ajax_update('/products/optimization/ajax/market')"
                                               class="btn-sm"
                                               name="market_status"
                                               value="Never"
                                               id="market2"
                                               autocomplete="off"> Add
                                    </label>
                                    <label class="btn btn-secondary borderRightRadius btn-sm">
                                        <input type="radio"
                                               onchange="ajax_update('/products/optimization/ajax/market')"
                                               class="btn-sm"
                                               name="market_status"
                                               value="Never"
                                               id="market3"
                                               autocomplete="off"> Gem
                                    </label>
                                </div>
                            @endif
                        </div>
                    <!-- selects -->
                        <div class="col-12 col-xl-6 d-flex pt-2 justify-content-between align-items-baseline">
                            <div class="mb-2">
                                <select name="device"
                                        onchange="ajax_update('/products/optimization/ajax/device')"
                                                id="device"
                                                class="form-control form-control-sm borderLeftRadius borderRightRadius"
                                >
                                    @if ($i->device == "")
                                        <option selected style="display: none;" value="">Device Type</option>
                                    @else
                                        <option disabled style="display: none;" value="">Device Type</option>
                                        <option selected style="display: none;" value="{{ $i->device }}">{{ $i->device }}</option>
                                    @endif

                                    @foreach ($device as $d)
                                        <option value="{{ $d->device }}">{{ $d->device }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <!-- picker -->
                            <div class="mb-2">
                                <button type="text" id="picker" class="btn btn-sm border dropdown-toggle form-control form-control-sm rounded-pill borderLeftRadius borderRightRadius">
                                    Date Range
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- icons -->
                <div class="groupIcons col-12 col-md-12 col-lg-12 col-xl-4 d-flex flex-column justify-content-around align-items-end p-0">
                    <!-- first row -->
                    <div class="row w-100 bg-light borderLeftRadius borderRightRadius p-3 mb-2">
                        <div class="col-2 p-0">
                            <form name="MyForm" method="POST" class="" id="optimization_complete">
                                @csrf
                                <i id="product-status" class="@if($i->child_product_id === null) product-status @endif fas fa-clipboard-check cursor-pointer
                                    @if( !empty($i->status))
                                        @if($i->status == "complete")
                                            text-success
                                        @else
                                            text-danger
                                        @endif
                                    @else
                                        text-danger
                                    @endif
                                    "
                                   data-toggle="popover"
                                   data-content="Status"
                                   data-placement="top"
                                   data-status="{{$i->status}}"
                                ></i>
                            </form>
                        </div>
                        <div class="col-3 p-0 text-center">
                            @if($priorID == "0")
                                <i class="fas fa-arrow-circle-left" style="color:lightslategray"
                                   data-toggle="popover"
                                   data-title="Previous"
                                   data-content="No previous product to display"
                                   data-placement="top"
                                   aria-hidden="true"></i>
                            @else
                                <a href="{{ route('products.optimization', $priorID) }}">
                                    <i class="fas fa-arrow-circle-left"
                                       style="color:#455a87"
                                       data-toggle="popover"
                                       data-content="Previous"
                                       data-placement="top"
                                       aria-hidden="true"></i>
                                </a>
                            @endif

                            @if($nextID == "0")
                                <i class="fas fa-arrow-circle-right" style="color:lightslategray" aria-hidden="true"
                                   data-toggle="popover"
                                   data-title="Next"
                                   data-content="No next product to display"
                                   data-placement="top"></i>
                            @else
                                <a href="{{ route('products.optimization', $nextID) }}">
                                    <i class="fas fa-arrow-circle-right" style="color:#455a87"
                                       data-toggle="popover"
                                       data-content="Next"
                                       data-placement="top"
                                       aria-hidden="true"></i>
                                </a>
                            @endif
                        </div>
                        <div class="col-4  p-0 text-center">
                            <i class="fas fa-plus-square text-bluetooth"
                               data-toggle="popover"
                               data-content="Add"
                               data-placement="top"></i>
                            <a
                                @if($i->child_product_id == null)
                                    href=" {{ route('products.optimization', $i->id) }}   "
                                @endif
                            >
                                <i class="fas fa-save text-success"
                                   data-toggle="popover"
                                   data-content="Save"
                                   data-placement="top"></i>
                            </a>
                        </div>
                        <div class="col-3 text-right p-0 ">
                            <a href="{{ route('products.list') }}">
                                <i class="far fa-times-circle text-danger"
                                   data-toggle="popover"
                                   data-content="Cancel"
                                   data-placement="top">
                                </i>
                            </a>
                            <a href="{{ route('products.delete', $i->id) }}" title="Delete" onclick="return confirm('Are you sure?')">
                                <i class="fas fa-trash-alt text-danger"
                                   data-toggle="popover"
                                   data-content="Delete"
                                   data-placement="top">
                                </i>
                            </a>
                        </div>
                    </div>
                    <!-- second row -->
                    <div class="row w-100 d-flex justify-content-between mb-2">
                        <div class="col-2 bg-light borderLeftRadius borderRightRadius p-3 d-flex justify-content-center">
                            <i class="fas fa-barcode"
                               data-toggle="popover"
                               data-content="Barcode"
                               data-placement="top"></i>
                        </div>
                        <div class="col-4 bg-light borderLeftRadius borderRightRadius p-3 d-flex justify-content-around">
                            <a
                                @if($i->child_product_id == null)
                                    href="{{ route('products.notifications.list', $i->id) }}"
                                @endif
                            >
                                <i class="fas fa-bell"
                                   data-toggle="popover"
                                   data-content="Notification"
                                   data-placement="top"
                                   aria-hidden="true"
                                    @if($notifications > 0)
                                        style="color:#8a1937"
                                    @else
                                        style="color:#377032"
                                    @endif
                                ></i>
                            </a>
                            <a
                                @if($i->child_product_id == null)
                                    href="{{ route('products.flags.list', $i->id) }}"
                                @endif
                            >
                                <i class="fas fa-flag text-danger"
                                   data-toggle="popover"
                                   data-content="Flags"
                                   data-placement="top"></i>
                            </a>
                            <!-- alarm icon bellow, need to replace in future, this was the similar and free -->
                            <a
                                @if($i->child_product_id == null)
                                    href="{{ route('products.alarm.edit', $i->id) }}"
                                @endif
                            >
                                <svg width="1.8em" height="1.8em"
                                     data-toggle="popover" data-content="Alarm"
                                     data-placement="top"
                                     viewBox="0 0 16 16" class="bi bi-alarm" fill="@if($alarms > '0')
                                        red
                                    @endif"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M8 15A6 6 0 1 0 8 3a6 6 0 0 0 0 12zm0 1A7 7 0 1 0 8 2a7 7 0 0 0 0 14z"/>
                                    <path fill-rule="evenodd" d="M8 4.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.053.224l-1.5 3a.5.5 0 1 1-.894-.448L7.5 8.882V5a.5.5 0 0 1 .5-.5z"/>
                                    <path d="M.86 5.387A2.5 2.5 0 1 1 4.387 1.86 8.035 8.035 0 0 0 .86 5.387zM11.613 1.86a2.5 2.5 0 1 1 3.527 3.527 8.035 8.035 0 0 0-3.527-3.527z"/>
                                    <path fill-rule="evenodd" d="M11.646 14.146a.5.5 0 0 1 .708 0l1 1a.5.5 0 0 1-.708.708l-1-1a.5.5 0 0 1 0-.708zm-7.292 0a.5.5 0 0 0-.708 0l-1 1a.5.5 0 0 0 .708.708l1-1a.5.5 0 0 0 0-.708zM5.5.5A.5.5 0 0 1 6 0h4a.5.5 0 0 1 0 1H6a.5.5 0 0 1-.5-.5z"/>
                                    <path d="M7 1h2v2H7V1z"/>
                                </svg>
                            </a>
                        </div>
                        <div class="col-5 bg-light borderLeftRadius borderRightRadius p-3 d-flex justify-content-around">
                            <i data-type="bluetooth"
                               data-status="{{ $i->bluetooth}}"
                               data-toggle="popover" data-content="Bluetooth"
                               data-placement="top"
                               class="
                                @if($i->child_product_id == null)
                                   remoteFeature
                                @endif
                               cursor-pointer fab fa-bluetooth
                                @if($i->bluetooth == 'on')
                                    text-bluetooth
                                @endif
                            "></i>
                            <i data-type="wifi"
                               data-status="{{$i->wifi}}"
                               data-toggle="popover" data-content="Wifi"
                               data-placement="top"
                               class="
                                @if($i->child_product_id == null)
                                   remoteFeature
                                @endif cursor-pointer fas fa-wifi
                                @if($i->wifi == 'on')
                                    text-warning
                                @endif
                            "></i>
                            <i data-type="rf"
                               data-status="{{$i->rf}}"
                               data-toggle="popover" data-content="Rf"
                               data-placement="top"
                               class="
                                @if($i->child_product_id == null)
                                   remoteFeature
                                @endif
                               cursor-pointer fas fa-broadcast-tower
                                @if($i->rf == 'on')
                                    text-success
                                @endif
                            "></i>
                            <i data-type="infrared"
                               data-status="{{$i->infrared}}"
                               data-toggle="popover" data-content="Infrared"
                               data-placement="top"
                               class="
                                @if($i->child_product_id == null)
                                   remoteFeature
                                @endif cursor-pointer fas fa-wifi
                                @if($i->infrared == 'on')
                                    text-danger
                                @endif
                            " style="transform: rotate(90deg);"></i>
                        </div>
                    </div>
                    <!-- third row  -->
                    <div class="row w-100 bg-light borderLeftRadius borderRightRadius px-1 py-2 d-flex justify-content-center align-items-center mb-2 mb-md-0 mb-lg-0">
                        <div class="col text-center">
{{--                            <button data-toggle="modal" data-target="#compatiblesModal" class="btn btn-block p-0">--}}
                            <a
                                @if($i->child_product_id == null)
                                    href="{{ route("products.compatibles.list", [$i->id])}}"
                                @else
                                    href="{{ route("products.compatibles.list", [$i->primary_product_id])}}"
                                @endif
                            >
                                <i class="far fa-list-alt text-success"
                                   data-toggle="popover"
                                   data-content="Compatibles"
                                   data-placement="top"></i>
                            </a>
                            {{--                            </button>--}}
                        </div>
                        <div class="col text-center">
                            <i class="fas fa-question text-primary
                            @if($i->child_product_id == null)
                                optimizationQuestion
                            @endif
                            "
                               data-toggle="popover"
                               data-content="Help"
                               data-placement="top"></i>
                        </div>
                        <div class="col text-center">
                            <a
                                @if($i->child_product_id == null)
                                    href="{{ route('products.searchlinks.main', [$i->id]) }}"
                                @endif
                               data-toggle="popover"
                               data-content="Search Links"
                               data-placement="top">
                                <img width="25rem" src="{{ asset('/images/icons/optimization/search.png')}}" alt="">
                            </a>
                        </div>
                        <div class="col text-center">
                            <button
                                @if($i->child_product_id == null)
                                    data-toggle="modal" data-target="#codeModal"
                                @endif
                                class="btn btn-block p-0">
                                <i class="fas fa-code text-dark"
                                   data-toggle="popover"
                                   data-content="Send to Developer"
                                   data-placement="top"></i>
                            </button>
                        </div>
                        <div class="col text-center">
                            <i class="fas fa-pen text-primary"
                               data-toggle="popover"
                               data-content="Edit"
                               data-placement="top"></i>
                        </div>
                        <div class="col text-center">
                            <button
                                @if($i->child_product_id == null)
                                    data-toggle="modal" data-target="#plainModal"
                                @endif
                                class="btn btn-block p-0">
                                <i class="fas fa-paper-plane colorLightBlue"
                                   data-toggle="popover"
                                   data-content="Assign to"
                                   data-placement="top"></i>
                            </button>
                        </div>
                        <div class="col text-center">
                            <a href="{{route('products.settings.list')}}">
                                <i class="fas fa-wrench text-warning
                                    @if($i->child_product_id == null)
                                        optimizationSettings
                                    @endif
                                    "
                                   data-toggle="popover"
                                   data-content="Settings"
                                   data-placement="top">
                                </i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{--alerts bellow--}}
<div class="row">
    <div class="col">
        <div class="mt-3 notification alert customAlert alert-success alert-dismissible fade show w-100" role="alert">
        </div>
        <div class="mt-3 alert alert-danger customAlert alert-dismissible fade show w-100" role="alert" >
        </div>
    </div>
</div>
{{-- modal for code message --}}
<div class="modal fade shadowGlobal codeModal" id="codeModal"  data-keyboard="false"  aria-labelledby="code" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
                <h5 class="modal-title text-center lightGrey py-2 px-4 border-radius-10" id="staticBackdropLabel">
                    <i class="fa-1x fas fa-code text-dark mr-3"></i> Send to developer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body codeModal">
                <form id="codeForm" method="post" class="w-100 p-5">
                    <div class="form-group mb-1">
                        @if($i->child_product_id === null)
                            <input id="id" type="hidden" name="id" value="{{ $i->id }}">
                        @else
                            <input id="id" type="hidden" name="id" value="{{ $i->primary_product_id }}">
                        @endif
                    </div>
                    <div class="form-group mb-1 pl-2">
                        <input id="bug" class="cursor-pointer" type="checkbox" name="bug" >
                        <label class="ml-1 cursor-pointer font-size-12 font-weight-bold text-dark" for="bug">Bug</label>
                    </div>
                    <div class="form-group mb-1 pl-2">
                        <input id="new_feature" class="cursor-pointer" type="checkbox" name="new_feature" >
                        <label class="ml-1 cursor-pointer font-size-12 font-weight-bold text-dark" for="new_feature">New Feature</label>
                    </div>
                    <div class="form-group mb-1 pl-2">
                        <input id="question" class="cursor-pointer" type="checkbox" name="question" >
                        <label class="ml-1 cursor-pointer font-size-12 font-weight-bold text-dark" for="question">Question</label>
                    </div>
                    <div class="form-group pl-2">
                        <input id="other" class="cursor-pointer" type="checkbox" name="other" >
                        <label class="ml-1 cursor-pointer font-size-12 font-weight-bold text-dark" for="other">Other</label>
                    </div>
                    <div class="form-group text-center mb-0">
                        <h4 class="text-center">Notes</h4>
                    </div>
                    <hr>
                    <div class="form-group mb-0">
                        <textarea placeholder="Comments" name="code_comments"  id="code_comments" class="w-100 border-radius-10 px-3 py-2" cols="30" rows="10"></textarea>
                    </div>
                    <div class="form-group text-center mb-0">
                        <button type="button" id="codeFormSubmit" class="btn colorLightBlue"><i class="fas fa-paper-plane colorLightBlue fa-3x"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade shadowGlobal" id="plainModal" data-keyboard="false"  aria-labelledby="plain" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
                <h5 class="modal-title text-center lightGrey py-2 px-5 border-radius-10" id="staticBackdropLabel">
                    Assign to:
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body plainModal">
                <form method="POST" class="w-100 p-5" id="plainForm">
                    <div class="row">
                        <div class="col d-flex flex-column justify-content-around align-items-start">
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn btn-block font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="far fa-images text-success mr-2"></i>
                                    <input type="checkbox" name="images" id="images"> Images
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fas fa-video mr-2 "></i>
                                    <input type="checkbox" name="videos" id="videos"> Videos
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fab fa-blogger text-primary mr-2"></i>
                                    <input type="checkbox" name="blog" id="blog"> Blog
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fas fa-book-open mr-2 text-dark"></i>
                                    <input type="checkbox" name="manuals" id="manuals"> Manuals
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="far fa-list-alt mr-2 text-dark"></i>
                                    <input type="checkbox" name="seo" id="seo"> SEO
                                </label>
                            </div>
                        </div>
                        <div class="col d-flex flex-column justify-content-center align-items-center">
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fas fa-database text-dark mr-2"></i>
                                    <input type="checkbox" name="data_check" id="data_check"> Data
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fas fa-dollar-sign dollarColor mr-2"></i>
                                    <input type="checkbox" name="price_check" id="price_check"> Price
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fab fa-facebook-square facebookColor mr-2"></i>
                                    <input type="checkbox" name="facebook" id="facebook"> Facebook
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fab fa-twitter twitterColor mr-2"></i>
                                    <input type="checkbox" name="twitter" id="twitter"> Twitter
                                </label>
                            </div>
                            <div class="btn-group-toggle plainButton mb-5 border-radius-10 hoverShadowScale w-100" data-toggle="buttons">
                                <label for="inventory" class="btn font-weight-bold font-size-12 d-flex justify-content-start">
                                    <i class="fas fa-edit text-warning font-size-12"></i>
                                    <input type="checkbox" name="inventory" id="inventory"> Inventory
                                </label>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0">
                                <textarea placeholder="Comments" name="comments" id="comments" class="w-100 border-radius-10 px-3 py-2" cols="30" rows="5"></textarea>
                            </div>
                            <div class="form-group text-center mt-3">
                                <button id="plainSubmit" class="btn bg-secondary rounded-pill ">save</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row">
                <div class="col">
                    <div class="modal-footer py-1">
                        <button type="button" class="btn btn-outline-danger rounded-pill " data-dismiss="modal">close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- middle of page graph pie chart product performance--}}
<section id="chart">
    <div class="row" >
        <div class="col-12 col-sm-12 col-md-12 col-lg-7 col-xl-7 mt-5">
            <div id="pieCharts" class="customChartCard border-radius-10">
                <div class="row justify-content-center align-items-center h-100">
                    <div class="col-9 mt-5">
                        <div class="row justify-content-center align-items-center h-100 p-3">
                            <div class="col-6 d-flex flex-column align-items-center">
                                <h3>Orders</h3>
                                <canvas id="orders" width="350px" height="300px" style=""></canvas>
                                <div class="align-self-end mr-5" style="margin-top: -2.5rem;">
                                    <h2 class="textOrganic mr-5 ">$</h2>
                                </div>
                            </div>
                            <div class="col-6 d-flex flex-column align-items-center">
                                <h3>Velocity</h3>
                                <canvas id="velocity" width="350px" height="300px"></canvas>
                                <div class="align-self-end mr-5" style="visibility: hidden; margin-top: -2.5rem;">
                                    <h2 class="textOrganic mr-5 ">$</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-3 d-flex flex-column align-items-center">
                        <div class="bg-secondary px-3 py-5 border-radius-10">
                            <div class="d-flex flex-row justify-content-start">
                                <div class="bg-colorAmazon legend mr-1"></div>
                                <p> Amazon</p>
                            </div>
                            <div class="d-flex flex-row justify-content-start">
                                <div class="bg-colorEbay legend mr-1"></div>
                                <p> Ebay</p>
                            </div>
                            <div class="d-flex flex-row justify-content-start">
                                <div class="bg-colorGoogle legend mr-1"></div>
                                <p> Google</p>
                            </div>
                            <div class="d-flex flex-row justify-content-start">
                                <div class="bg-colorOrganic legend mr-1"></div>
                                <p> Organic</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-5">
                    <div class="col-9 d-flex flex-column justify-content-center align-items-center">
                    {{-- This will be generated with js after function ajax call and data will be displayed from the backend   --}}
                        <div class="w-75">
                            <div class="progress" style="height: 20px">
                                <div class="progress-bar bg-success" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 30%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar bg-success" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar bg-danger" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                        <div class="row w-75">
                            <div class="col ">From: <span id="fromRange"> Oct</span></div>
                            <div class="col text-right">To: <span id="toRange"> Sept</span></div>
                        </div>
                        <div class="p-3 h4">
                            125/365 Days active in market
                        </div>
                    </div>
                </div>
                <div class="row my-5">
                    <div class="col-9 text-center">
                        <div class="form-group input-group-md d-flex justify-content-center align-items-center">
                            <input type="checkbox" name="data" class="" id="data">
                            <label for="data" class="ml-2 mb-0 font-size-12">
                                Data for all conditions
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-12 col-lg-5 col-xl-5 mt-5">
            <div id="futureUse" class="card customChartCard border-radius-10">
                <div class="card-body d-flex justify-content-center align-items-center">
                    <h1 class="text-question text-center">Future use</h1>
                </div>
            </div>
        </div>
    </div>
</section>
    {{-- bottom of page --}}
<section id="chartBottom">
    <div class="row mt-5 justify-content-start">
            {{-- Amazon box Card--}}
        <div class="col-12 col-lg-4 col-md-6 col-sm-6 col-xl-3 w-100 mb-5">
            <div class="bg-white border-radius-10 customBottomCard card">
                {{--     First row           --}}
                <div class="row align-items-center justify-content-around w-100 card-title">
                    <div class="col">
                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/amazon.png')}}" alt="">
                    </div>
                    <div class="col text-right">
                        <div class="form-control-lg custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input bg-success cursor-pointer" id="amazonSwitch">
                            <label class="custom-control-label cursor-pointer" id="labelAmazon" for="amazonSwitch">Off</label>
                        </div>
                    </div>
                </div>
                {{--Second Row--}}
                <div class="card-body p-1">
                    <div class="row mt-3 align-items-center">
                        <div class="col-8">
                            <div class="form-group my-1">
                                <input class="form-control" type="text" placeholder="Price" id="amazonPrice">
                            </div>
                        </div>
                        <div class="col-4 d-flex flex-row justify-content-around align-items-center">
                            <i class="fas fa-gift text-warning mr-2"></i>
                            <div class="h-75">
                                <img class="w-100 h-100" src="{{ asset('/images/icons/optimization/customImages/arrow_down_dollar.png')}}" alt="arrowdowndollar">
                            </div>
                        </div>
                    </div>
                    {{--  Third Row --}}
                    <div class="row align-items-center">
                        <div class="col-10">
                            <div class="form-group my-1">
                                <input class="form-control " type="text" placeholder="ASIN" id="ASINAmazon">
                            </div>
                        </div>
                        <div class="col-2">
                            <i class="text-secondary fas fa-search cursor-pointer"></i>
                        </div>
                    </div>
                    {{--    Forth Row            --}}
                    <div class="row">
                        <div class="col-12 d-flex flex-column">
                            <div class="h5">Impr.</div>
                            <div class="h5">CTR.</div>
                        </div>
                    </div>
                </div>
                {{--Fifth Row --}}
                <a class="questionSign" href="{{ route('products.amazon.details', [$i->id]) }}">
                    <i class="fas fa-question text-question"></i>
                </a>
            </div>
        </div>
            {{--   Ebay start     --}}
        <div class="col-12 col-lg-4 col-md-6 col-sm-6 col-xl-3 w-100 mb-5">
            <div class="bg-white border-radius-10 customBottomCard card">
                {{--     First row           --}}
                <div class="row align-items-center justify-content-between w-100 card-title">
                    <div class="col-6">
                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/ebay.png')}}" alt="">
                    </div>
                    <div class="col-3 text-right">
                        <div class="form-control-lg custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input bg-success cursor-pointer" id="ebaySwitch">
                            <label class="custom-control-label cursor-pointer" id="labelEbay" for="ebaySwitch">Off</label>
                        </div>
                    </div>
                    <div class="col-3 text-right">
                        <i class="fas fa-eye text-dark font-size-12"></i>
                    </div>
                </div>
                {{--Second Row--}}
                <div class="card-body p-1">
                    <div class="row mt-3 align-items-center">
                        <div class="col-8">
                            <div class="form-group my-1">
                                <input class="form-control" type="text" placeholder="Price" id="ebayPrice">
                            </div>
                        </div>
                    </div>
                    {{--    Third Row            --}}
                    <div class="row">
                        <div class="col">
                           <p class="h4">Marketability</p>
                        </div>
                    </div>
                    {{-- forth row--}}
                    <div class="row">
                        <div class="col-10 d-flex flex-column">
                            <div class="h5">Active: <span>39</span></div>
                            <div class="h5">Sold: <span>14</span></div>
                            <div class="h5">Not Sold: <span>9</span></div>
                        </div>
                    </div>
                </div>
                {{--  fifth row --}}
                <a class="questionSign" href="#">
                    <i class="fas fa-question text-question"></i>
                </a>
            </div>
        </div>
            {{--   Google start     --}}
        <div class="col-12 col-lg-4 col-md-6 col-sm-6 col-xl-3  w-100 mb-5">
            <div class="bg-white border-radius-10 customBottomCard card">
                {{--     First row           --}}
                <div class="row align-items-center justify-content-around w-100 card-title">
                    <div class="col-6">
                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/google.png')}}" alt="">
                    </div>
                    <div class="col-3 text-right">
                        <div class="form-control-lg custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input bg-success cursor-pointer" id="googleSwitch">
                            <label class="custom-control-label cursor-pointer" id="labelGoogle" for="googleSwitch">Off</label>
                        </div>
                    </div>
                    <div class="col-3 text-right">
                        <i class="fas fa-eye text-dark font-size-12"></i>
                    </div>
                </div>
                {{--Second Row--}}
                <div class="card-body p-1">
                    <div class="row">
                        <div class="col-12 d-flex flex-column">
                            <div class="h5">CPC</div>
                            <div class="h5">Clicks</div>
                            <div class="h5">Impr.</div>
                            <div class="h5">Cost/Conv</div>
                            <div class="h5">Cost</div>
                            <div class="h5">CTR</div>
                        </div>
                    </div>
                </div>
                <a class="questionSign" href="#">
                    <i class="fas fa-question text-question"></i>
                </a>
            </div>
        </div>
            {{--    Google Organic    --}}
        <div class="col-12 col-lg-4 col-md-6 col-sm-6 col-xl-3  w-100 mb-5">
            <div class="bg-white border-radius-10 customBottomCard card">
                {{--     First row           --}}
                <div class="row align-items-center justify-content-around w-100 card-title">
                    <div class="col">
                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/google.png')}}" alt="">
                    </div>
                    <div class="col text-right">
                        <div class="d-flex flex-row align-items-center justify-content-around">
                            <div class="h4 mb-0">Organic</div>
                            <i class="text-success fas fa-leaf"></i>
                        </div>
                    </div>
                </div>
                {{--Second Row--}}
                <div class="card-body p-1">
                    <div class="row mt-3 align-items-center">
                        <div class="col-8">
                            <div class="form-group my-1">
                                <input class="form-control" type="text" placeholder="Price" id=googleOrganicPrice>
                            </div>
                        </div>
                    </div>
                    {{--  Third Row --}}
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="h2 text-success">Position: 3</div>
                        </div>
                    </div>
                    {{--    Forth Row            --}}
                    <div class="row my-5">
                        <div class="col d-flex flex-row">
                            <img width="25rem" src="{{ asset('/images/icons/optimization/search.png')}}" alt="">
                            <i class="far fa-list-alt ml-5 text-dark"></i>
                        </div>
                    </div>
                </div>
                <a class="questionSign" href="#">
                    <i class="fas fa-question text-question"></i>
                </a>
            </div>
        </div>
        {{--   Future --}}
        {{--        Amazon Prime --}}
        <div class="col-12 col-lg-4 col-md-6 col-sm-6 col-xl-3 w-100 mb-5">
            <div class="bg-white border-radius-10 customBottomCard card">
                {{--     First row           --}}
                <div class="row align-items-center justify-content-between w-100 card-title">
                    <div class="col-6">
                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/amazon-prime.png')}}" alt="">
                    </div>
                    <div class="col-3 text-right">
                        <i class="fas fa-eye text-dark font-size-12"></i>
                    </div>
                </div>
                {{--Second Row--}}
                <div class="card-body p-1">
                    <div class="row mt-3 align-items-center">
                        <div class="col-8">
                            <div class="form-group my-1">
                                <input class="form-control" type="text" placeholder="Price" id="amazonPrimePrice">
                            </div>
                        </div>
                        <div class="col-4 d-flex flex-row justify-content-around align-items-center">
                            <i class="fas fa-gift text-warning mr-2"></i>
                            <div class="h-75">
                                <img class="w-100 h-100" src="{{ asset('/images/icons/optimization/customImages/arrow_down_dollar.png')}}" alt="arrowdowndollar">
                            </div>
                        </div>
                    </div>
                    {{--  Third Row --}}
                    <div class="row align-items-center">
                        <div class="col-10">
                            <div class="form-group my-1">
                                <input class="form-control " type="text" placeholder="ASIN" id="ASINAmazonPrice">
                            </div>
                        </div>
                        <div class="col-2">
                            <i class="text-secondary fas fa-search cursor-pointer"></i>
                        </div>
                    </div>
                    {{--    Forth Row            --}}
                    <div class="row">
                        <div class="col-12 d-flex flex-column">
                            <div class="h4">Impr.</div>
                            <div class="h4">CTR.</div>
                        </div>
                    </div>
                </div>
                {{--Fifth Row --}}
                <a class="questionSign" href="#" >
                    <p class="text-question h3" style="text-decoration: line-through;"> Future Use<i class="fas fa-question text-question"></i></p>
                </a>
            </div>
        </div>
        {{-- Bing--}}
        <div class="col-12 col-lg-4 col-md-6 col-sm-6 col-xl-3 w-100 mb-5">
            <div class="bg-white border-radius-10 customBottomCard card">
                {{--     First row           --}}
                <div class="row align-items-center justify-content-between w-100 card-title">
                    <div class="col-6">
                        <img class="main" src="{{ asset('/images/icons/optimization/customImages/bing.png')}}" alt="">
                    </div>
                    <div class="col-6 text-right">
                        <div class="d-flex flex-row align-items-center justify-content-around">
                            <div class="h4 mb-0">Organic</div>
                            <i class="text-success fas fa-leaf"></i>
                        </div>
                    </div>
                </div>
                {{--Second Row--}}
                <div class="card-body p-1">
                    <div class="row mt-3 align-items-center">
                        <div class="col-8">
                            <p class="h2 text-success">Position: 3</p>
                        </div>
                    </div>
                </div>
                {{--Fifth Row --}}
                <a href="#" class="questionSign">
                    <p class="text-question h3" style="text-decoration: line-through;"> Future Use<i class="fas fa-question text-question"></i></p>
                </a>
            </div>
        </div>
    </div>
</section>
@endforeach
@endsection

{{-- Scripts Section --}}
@section('scripts')
    {{--Charts scripts & cdn's--}}
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.21/jquery.zoom.js"></script>

    <script src="/js/customJs/optimization/modals/codeForm.js"></script>
    <script src="/js/customJs/optimization/modals/plainForm.js"></script>
    <script src="/js/customJs/optimization/bin_qoh_loc/index.js"></script>
    <script src="/js/customJs/optimization/chart/index.js"></script>

    <script src="/js/customJs/optimization/index.js"></script>
    <script src="/js/customJs/optimization/indexVanila.js"></script>
@endsection

