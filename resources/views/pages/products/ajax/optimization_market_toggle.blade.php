<div class="btn-group btn-group-toggle" data-toggle="buttons">
    <label class="btn
    @if($market->status == "Never")
        btn-success active
    @else
        btn-secondary
    @endif
    ">
        <input type="radio"
               name="market_status"
               value="Never"
               id="market1"
               autocomplete="off"
               onclick="ajax_update(this.form, '{{ route('products.ajax.market') }}', '#ajax_market');"
                    @if($market->status == "Never")
                        checked
                    @endif
        > Never
    </label>
    <label class="btn
    @if($market->status == "Add")
        btn-success active
    @else
        btn-secondary
    @endif
    ">
        <input type="radio"
               name="market_status"
               value="Add"
               id="market2"
               autocomplete="off"
               onclick="ajax_update(this.form, '{{ route('products.ajax.market') }}', '#ajax_market');"
                    @if($market->status == "Add")
                        checked
                    @endif
        > Add
    </label>
    <label class="btn
    @if($market->status == "Gem")
        btn-success active
    @else
        btn-secondary
    @endif
    ">
        <input type="radio"
               name="market_status"
               value="Gem"
               id="market3"
               autocomplete="off"
               onclick="ajax_update(this.form, '{{ route('products.ajax.market') }}', '#ajax_market');"
                    @if($market->status == "Gem")
                        checked
                    @endif
        > Gem
    </label>
</div>
