{{-- Extends layout --}}
@extends('layout.default')

{{-- styling --}}
@section('styles')
    <link rel="stylesheet" href="/css/dropzone.css">
    <link rel="stylesheet" href="/css/customCss/style.css">
    <link rel="stylesheet" href="/css/customCss/pages.css">
<style>
    /* Container holding the image and the text */
    .container {
        position: relative;
        text-align: center;
        color: white;
    }
    /* Top right text */
    .top-right {
        position: absolute;
        top: -5px;
        right: 16px;
    }
</style>
@endsection
{{-- Content --}}
@section('content')
<div class="card card-custom">
    <div class="card-header flex-wrap border-0 pt-6 pb-0">
        <div class="card-title">
            <h3 class="card-label">Products</h3>
        </div>
    </div>
    <div class="card-body">
        <!--begin::Search Form-->
        <div class="mt-2 mb-5 mt-lg-5 mb-lg-10">
            <form action="{{ route('products.search') }}" method="GET">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" value="{{ $search_text }}" name="search_text" class="form-control" placeholder="Search..." id="search_text" />
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>

                            {{--
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Status:</label>
                                    <select class="form-control" id="kt_datatable_search_status">
                                        <option value="">All</option>
                                        <option value="1">Pending</option>
                                        <option value="2">Delivered</option>
                                        <option value="3">Canceled</option>
                                        <option value="4">Success</option>
                                        <option value="5">Info</option>
                                        <option value="6">Danger</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Type:</label>
                                    <select class="form-control" id="kt_datatable_search_type">
                                        <option value="">All</option>
                                        <option value="1">Online</option>
                                        <option value="2">Retail</option>
                                        <option value="3">Direct</option>
                                    </select>
                                </div>
                            </div>
                            --}}
                            <div class="col-md-4 my-2 my-md-0">
                                <input type="submit" value="Search" class="btn btn-light-primary px-6 font-weight-bold">&nbsp;
                                <a href="{{ route('products.list') }}" class="btn btn-light-warning px-6 font-weight-bold">Reset</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!--end::Search Form-->
        @if ($message = session('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $message }}
        </div>
        @endif

        @if ($error = session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $error }}
        </div>
        @endif

        @if ($warning = session('warning'))
        <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            {{ $warning }}
        </div>
        @endif

        {{ $data->appends(request()->only('_id'))->withQueryString()->links() }}

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th>Thumbnail</th>
                    <th>SKU</th>
                    <th>Bin</th>
                    <th>Brand</th>
                    <th>MPN</th>
                    <th>Store</th>
                    <th>Amazon</th>
                    <th>Ebay</th>
                    <th>Qty</th>
                    <th>Condition</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @if ($data->count())
                @foreach ($data as $i)
                <!-- modal -->
                <tr>
                    <td>
                        <div class="modal" id="productModal{{$i->id}}" tabindex="-1" role="dialog" aria-labelledby="{{$i->id}}" aria-hidden="true">
                            <div class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title text-center"  id="{{ $i->id}}">Existing images </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <!-- code to work here  -->
                                    <div class="modal-body">
                                        <div class="container">
                                            <!-- existing images need to go in loop -->
                                            <div class="row py-5 border-top border-bottom d-flex justify-content-start" id="productImages{{$i->id}}">
                                            </div>
                                            <!-- dropzone -->
                                            <div class="row my-5">
                                                <div class="col" style="position: relative;">
                                                    <input name="csrf-token" type="hidden" value="{{ csrf_token() }}" />
                                                    <form class="dropzone" id="myId{{$i->id}}"  action="{{ route('product.imageupload.save') }}" method="POST" enctype="multipart/form-data">
                                                        <div class="dz-message" style="color: black;">
                                                            <div class="fallback">
                                                                <input type="hidden" name="type" value="secondary">
                                                                <input class="file" type="file" name="file" />
                                                            </div>
                                                            <i class="fas fa-cloud-upload-alt fa-4x"></i>
                                                            <p>Upload up to 3 photos</p>
                                                            <p>Drag or click to upload</p>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <!-- capture preview -->
                                            <div class="row my-5 border-top border-bottom py-5 justify-content-between align-items-center">
                                                <div class="col d-flex justify-content-center">
                                                    <div data-id="{{$i->id}}" id="my_camera{{$i->id}}" class="my_camera"></div>
                                                </div>
                                                {{-- <form action="{{ route('product.imageupload.save') }}" method="POST" enctype="multipart/form-data"> --}}
                                                    {{-- @csrf --}}
                                                    <input type="hidden" name="id" value="{{ $i->id }}">
                                                    <input type="hidden" name="type" value="secondary">
                                                    <div class="row">
                                                        <div class="col">
                                                        <input style="visibility: hidden" type="hidden" id="file{{$i->id}}" name="file" class="image-tag" >
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    {{-- <button data-id="{{$i->id}}" type="submit" class="btn btn-success submitForm" id="submitForm{{$i->id}}">Save</button> --}}
                                                    </div>
                                                {{-- </form> --}}
                                                <div class="col d-flex justify-content-center">
                                                    <div data-id="{{$i->id}}" id="preview{{$i->id}}" class="preview"></div>
                                                </div>
                                            </div>
                                            <div class="row my-5">
                                                <div class="col text-left d-flex justify-content-start align-items-center">
                                                    <i data-product="{{$i->id}}" class="cursor-pointer openCamera mr-3 fas fa-camera fa-3x text-primary" style="font-size: 2rem;"></i>
                                                    <button data-product="{{$i->id}}" id="takeSnap{{$i->id}}" type="button" class="btn btn-primary takeSnap" onClick="take_snapshot()">Take Snapshot</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
                <!-- end modal -->

                <tr class="@if( $i->child_product_id ) bg-lightYellow @endif ">
                    <td>
                        <a href="{{ route('products.optimization', $i->id) }}">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                        </a>
                    </td>
                    <td>
                        @if(isset($i->product_id))
                                <img data-id="{{$i->id}}"
                                     data-open-count="0"
                                     class="cursor-pointer image @if($i->child_product_id === null) openModal @endif"
                                     src="{{ $i->url_thumbnail }}" width="51" height="90">
                        @else
                        {{-- New Product --}}
                        {{-- #OD-25 please style "new item" here --}}
                        <div class="container">
                            <img
                                data-id="{{$i->id}}"
                                data-open-count="0"
                                class="cursor-pointer @if($i->child_product_id === null) openModal @endif image"
                                @if($i->url_thumbnail != "")
                                    src="{{ $i->url_thumbnail }}"
                                @elseif($i->url_thumbnail2 != "")
                                    src="{{ $i->url_thumbnail2 }}"
                                @endif
                                width="90"
                                height="51">
                            <div class="top-right text-white new-item ">
                                <span class="text-white">new item</span>
                            </div>
                        </div>
                        @endif
                    </td>
                    <td>{{ $i->sku }}</td>
                    <td>{{ $i->bin_picking_number }}</td>
                    <td>{{ $i->brand_name }}</td>
                    <td>{{ $i->mpn }}</td>
                    <td>{{ $i->price }}</td>
                    <td>TBD</td>
                    <td>TBD</td>
                    <td>
                        @if ($i->inventory_level == "")
                        0
                        @else
                        {{ $i->inventory_level }}
                        @endif
                    </td>
                    <td>{{ $i->product_condition }}</td>
                    <td class="nowrap">
                        <a href="{{ route('products.delete', $i->id) }}" onclick="return confirm('You are about to delete this product.')" class="btn btn-sm btn-circle btn-outline-danger"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
{{--                <tr class="collapse" id="opt_{{ $i->id }}">--}}
{{--                    <td class="nopadding" colspan="12">--}}
{{--                        test--}}
{{--                    </td>--}}
{{--                </tr>--}}
                @endforeach
                @endif

            </tbody>
        </table>
        {{ $data->appends(request()->only('_id'))->withQueryString()->links() }}
    </div>
</div>
@endsection


{{-- Scripts Section --}}
@section('scripts')
{{-- vendors --}}
<script src="/js/dropzone.js"></script>
<script src="/js/webcam.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="/js/customJs/imageEdit.js"></script>
@endsection
