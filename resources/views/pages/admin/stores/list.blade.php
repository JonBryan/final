@extends('layout.default')

@section('styles')
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card card-custom">
                <div class="card-header flex-wrap border-0 pt-6 pb-0">
                    <div class="card-title">
                        <h3 class="card-label">Admin : Manage Stores</h3>
                    </div>
                    <div class="card-toolbar">
                        <!--begin::Button-->
                        <a href="{{ route('admin.stores.new') }}" class="btn btn-primary font-weight-bolder">
                            <span class="svg-icon svg-icon-md">
                                <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                     version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"/>
                                        <circle fill="#000000" cx="9" cy="15" r="6"/>
                                        <path
                                            d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                            fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>New</a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <div class="row my-5">
                        <div class="col-12 col-md-12 col-lg-8">
                            <form method="GET" action="{{route('admin.stores.list')}}">
                                <div class="form-row">
                                    <div class="col-12 col-md-6 col-lg-6">
                                        <input class="form-control" name="search" id="search" type="search" placeholder="Search for store">
                                    </div>
                                    <div class="col-6 col-md-3 col-lg-3 d-flex flex-row ">
                                        <button type="submit" class="form-control btn btn-success mr-2">Search</button>
                                    </div>
                                    <div class="col-6 col-md-3 col-lg-3">
                                        <a href="{{route('admin.stores.list')}}" class="form-control btn btn-info">Show all</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    @if ($message = session('message'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            {{ $message }}
                        </div>
                    @endif
                    <div class="row">
                        <div class="col">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Store ID</th>
                                    <th>Store Name</th>
                                    <th>Store URL</th>
                                    <th>Store Client ID</th>
                                    <th>Store Token</th>
                                    <th>Main?</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stores as $store)
                                    <tr>
                                        <td>{{$store->store_id}}</td>
                                        <td>{{$store->name}}</td>
                                        <td>{{$store->url}}</td>
                                        <td>{{$store->client_id}}</td>
                                        <td>{{$store->token}}</td>
                                        <td>{{$store->is_main ? 'Yes' : 'NO'}}</td>
                                        <td>
                                            <a href="{{route('admin.stores.edit', $store->id)}}" class="btn btn-warning btn-sm">
                                                Edit
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col d-flex justify-content-center margin-y-30">
                            {{ $stores->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
@endsection
