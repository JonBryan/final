{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Admin : Users : Edit</h3>
            </div>
        </div>

        <div class="card-body">
            @if ($message = session('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $message }}
                </div>
            @endif

            @if ($error = session('error'))
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $error }}
                </div>
            @endif

            @if ($warning = session('warning'))
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    {{ $warning }}
                </div>
            @endif
            <form action="{{ route('admin.users.update') }}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="name">Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    id="name"
                                    value="{{ $data->name }}"
                                    class="form-control"
                                    required="required"
                                    data-lpignore="true"
                                >
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="email">Email</label>
                                <input
                                    type="text"
                                    name="email" id="email" class="form-control"
                                    required="required"
                                    value="{{ $data->email }}"
                                    data-lpignore="true"
                                >
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <div class="alert alert-info">
                                    If you would like to change the password please enter in a new
                                    password or leave the field blank.
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="password">New Password</label>
                                <input
                                    type="text"
                                    name="password"
                                    class="form-control"
                                    id="password"
                                    data-lpignore="true"
                                >
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-8">
                                <label for="roles">Security Group</label>
                                <select
                                    name="roles[]"
                                    class="form-control"
                                    id="roles"
                                    multiple
                                    size="5"
                                    required="required"
                                    data-lpignore="true"
                                >
                                    <option value="">Select one or more</option>
                                    <optgroup
                                        label="----------------------------------------------"></optgroup>
                                    @if ($roles->count())
                                        @foreach ($roles as $index => $i)
                                            @php
                                                $roleSelected = "0";
                                            @endphp
                                            @foreach($attachedRoles as $k => $v)
                                                @if ($v == $i->name)
                                                    @php
                                                        $roleSelected = "1";
                                                    @endphp
                                                @endif
                                            @endforeach
                                            @if ($roleSelected == "1")
                                                <option selected
                                                        value="{{ $i->slug }}">{{ $i->name }}</option>
                                            @else
                                                <option value="{{ $i->slug }}">{{ $i->name }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" class="btn btn-success" value="Save">&nbsp;
                        <a href="{{ route('admin.users.list') }}" class="btn btn-warning">Cancel</a>
                    </div>
                </div>
            </form>
        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
