@extends('layout.default')


@section('content')
<div class="row">
    <div class="col">
        <div class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <div class="card-title">
                    <h3 class="card-label">Bulk : Bulk Updates</h3>
                </div>
            </div>
            <card class="card-body">
                <div class="row">
                    <div class="col-2">
                        <ul class="list-unstyled">
                            <li class="">
                                <a role="button"
                                    class="d-flex align-items-center font-size-15 cursor-pointer btn btn-lg"
                                    data-toggle="modal" data-target="#settingsModal"
                                    >
                                    <i class="fas fa-cog mr-2"></i><span class=" text-muted"> Settings</span>
                                </a>
                            </li>
                            <li class="">
                                <a role="button"
                                    class="d-flex align-items-center font-size-15 cursor-pointer btn btn-lg"
                                    data-toggle="modal" data-target="#actionModal"
                                    >
                                    <i class="far fa-hand-paper mr-2"></i><span class=" text-muted"> Updates</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-7">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th><input style="width: 15px; height: 15px;" type="checkbox" name="select_all" id="select_all"></th>
                                    <th>SKU</th>
                                    <th>MPN</th>
                                    <th>TITLE</th>
                                    <th>BULLET POINTS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" name="" id=""></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                    <td><input type="text" placeholder="Type Here..."></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-3 d-flex justify-content-end h-100 position-relative">
                            <a role="button"
                                id="filter"
                                class="d-flex font-size-15 cursor-pointer btn btn-lg"
                                data-toggle="collapse"
                                data-target="#filterBox"
                                aria-expanded="false" aria-controls="collapseExample"
                                {{-- data-toggle="modal" data-target="#filterModal" --}}
                            >
                                <i class="fas fa-filter"></i>
                                <span class="">FILTER</span>
                            </a>
                            <div class="collapse border" id="filterBox">
                                <i class="fas fa-sort-up"></i>
                                filter box
                            </div>
                    </div>
                </div>
            </card>
        </div>
    </div>
</div>
{{-- settings modal start --}}
<div class="modal fade" id="settingsModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
                <h3 class="text-center" id="exampleModalLabel">Settings</h3>
            </div>
            <div class="modal-body bulkSettingsAction" id="">
                <div class="row align-align-items-center flex-column flex-md-row flex-lg-row minHeight-300 justify-content-md-between justify-content-lg-between px-5">
                    <div class="col-12 col-md-5 col-lg-5 h-100 p-0 my-3">
                        <div class="col text-center btnMainCat active mb-3 ">
                            <button class="platform btn btn-block h4">Not Active</button>
                        </div>
                        <select name="available_flags[]" id="available_flags" multiple class="form-control min-height-300">
                            <option class="p-1 h5 border-bottom" value="">Brand</option>
                            <option class="p-1 h5 border-bottom" value="">Bin</option>
                            <option class="p-1 h5 border-bottom" value="">Additional info</option>
                        </select>
                    </div>
                    {{-- buttons --}}
                    <div id="" class="col-12 col-md-1 col-lg-1 d-flex flex-row flex-md-column flex-flex-lg-column justify-content-center align-items-center my-3">
                        <button class="save btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-double-right"></i></button>
                        <button id="" class="save btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-right"></i></button>
                        <button id='' class="delete btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-left"></i></button>
                        <button class="delete btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-double-left"></i></button>
                    </div>

                    <div class="col-12 col-md-5 col-lg-5 h-100 p-0 my-3">
                        <div class="col text-center btnMainCat active mb-3">
                            <button class="platform btn btn-block h4">Active / Selected</button>
                        </div>
                        <select name="active_flags[]" id="active_flags" multiple class="form-control min-height-300">
                            <option class="p-1 h5 border-bottom" value="">MPN</option>
                            <option class="p-1 h5 border-bottom" value="">SKU</option>
                            <option class="p-1 h5 border-bottom" value="">BULLET POINT</option>
                            <option class="p-1 h5 border-bottom" value="">TITLE</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Action modal start --}}
<div class="modal fade" id="actionModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" open=open>
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
            <h5 class="modal-title" id="exampleModalLabel">Action</h5>
            </div>
            <div class="modal-body bulkSettingsAction">
                <div class="row flex-column justify-content-center my-5 px-5 ">
                    <div class="col-2">
                        <div class="text-center btnMainCat hoverBtnMain mb-2">
                            <button class="platform btn btn-block">All</button>
                        </div>
                        <div class="text-center btnMainCat hoverBtnMain mb-2">
                            <button class="platform btn btn-block ">Amazon</button>
                        </div>
                        <div class="text-center btnMainCat mb-2 hoverBtnMain">
                            <button class="platform btn btn-block ">Ebay</button>
                        </div>
                        <div class="text-center btnMainCat mb-2 hoverBtnMain">
                            <button class="platform btn btn-block">Shopping</button>
                        </div>
                        <div class="text-center btnMainCat mb-2 hoverBtnMain">
                            <button class="platform btn btn-block">General</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- filter modal --}}
{{-- <div class="modal fade" id="filterModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" open=open>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header d-flex justify-content-center">
            <h5 class="modal-title" id="exampleModalLabel">Available Filters</h5>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div> --}}

@endsection


@section('scripts')
@endsection
