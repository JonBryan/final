{{-- Extends layout --}}
@extends('layout.default')

{{-- CSS --}}
@section('styles')
@endsection

{{-- Content --}}
@section('content')

<div class="row">
    <input name="csrf-token" type="hidden" value="{{ csrf_token() }}" />
    <input type="hidden" id="product_id" value="{{$id}}">
    <input type="hidden" id="allFlags" value="{{$flags}}">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12">
        <p id="successAlert" class="alert customAlert alert-success"></p>
        <div id="flagsCard" class="card card-custom">
            <div class="card-header flex-wrap border-0 pt-6 pb-0 d-flex justify-content-between">
                <div class="card-title">
                    <h3 class="card-label"><a href="{{ route('products.list') }}">Products</a> :: <a href="{{ route('products.optimization', $product->id) }}">{{ $product->brand_name }}</a> :: Flags</h3>
                </div>
            </div>
            <div class="card-body" style="min-height: 60vh;">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-9">
                        <h1 class="font-weight-bold">Flags</h1>
                    </div>
                </div>
                <!-- heading Titles -->
                <div class="row flex-column flex-md-row flex-flex-lg-row justify-content-center my-5 px-5 ">
                    <div class="col text-center btnMainCat active">
                        <button class="platform btn btn-block">All</button>
                    </div>
                    <div class="col text-center btnMainCat">
                        <button class="platform btn btn-block ">Amazon</button>
                    </div>
                    <div class="col text-center btnMainCat">
                        <button class="platform btn btn-block ">Ebay</button>
                    </div>
                    <div class="col text-center btnMainCat">
                        <button class="platform btn btn-block">Shopping</button>
                    </div>
                    <div class="col text-center btnMainCat">
                        <button class="platform btn btn-block">General</button>
                    </div>
                </div>
                <!-- end Heading Titles -->
                <div class="row align-align-items-center flex-column flex-md-row flex-lg-row minHeight-300 justify-content-md-between justify-content-lg-between px-5">
                    <div class="col-12 col-md-5 col-lg-5 h-100 p-0 my-3">
                        <div>
                            <h3 class="text-center border mb-0 p-3 w-100"> Inactive flags</h3>
                        </div>
                        <select name="available_flags[]" id="available_flags" multiple class="form-control h-100 w-100">
                            @foreach ($flags as $flag)
                            <option class="p-1 h5 border-bottom" value="{{ $flag->id }}">{{ $flag->name }} : {{ $flag->platform }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="btnActionFlags" class="col-12 col-md-1 col-lg-1 d-flex flex-row flex-md-column flex-flex-lg-column justify-content-center align-items-center my-3">
                        <button class="save btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-double-right"></i></button>
                        <button id="" class="save btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-right"></i></button>
                        <button id='' class="delete btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-left"></i></button>
                        <button class="delete btn btn-light my-1"><i class="rotateIcons text-muted fas fa-angle-double-left"></i></button>
                    </div>
                    <div class="col-12 col-md-5 col-lg-5 h-100 p-0 my-3">
                        <div>
                            <h3 class="text-center border mb-0 p-3">Active flags</h3>
                        </div>
                        @if (!empty($data))
                        <select name="active_flags[]" id="active_flags" multiple class="form-control h-100 w-100">
                            @foreach ($data as $d)
                            <option class="p-1 h5 border-bottom" value="{{ $d->admin_flag_id }}">{{ $d->name }} : {{ $d->platform }}</option>
                            @endforeach
                        </select>
                        @endif

                        <!-- @foreach($data as $d)
                        {{ $d->name  }} :: {{ $d->platform }}<br>

                        @endforeach -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- Scripts Section --}}
@section('scripts')
{{-- vendors --}}
<script src="/js/customJs/optimization/optimizationFlags.js"></script>
@endsection
