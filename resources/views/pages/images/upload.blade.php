{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Test Image Upload</h3>
            </div>
        </div>

        <div class="card-body">

            @if ($message = session('message'))
                <div class="alert alert-success">{{ $message }}</div>
            @endif
            <form action="{{ route('product.imageupload.save') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" value="{{ $id }}">
                <input type="hidden" name="type" value="secondary">
                <div class="row mt-2">
                    <div class="col-8">
                        <label for="image">Image</label>
                        <input type="file" name="image" id="image">
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-8">
                        <input type="submit" value="Upload" class="btn btn-primary">&nbsp;
                    </div>
                </div>
            </form>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}

@endsection
