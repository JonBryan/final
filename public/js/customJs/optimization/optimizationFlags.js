
// =============================  flags =================================
$(document).ready(function() {
    $('#successAlert').hide()
    $('.platform').click(function(e){
        let AllFlags = JSON.parse($('#allFlags').val())
        let platform = e.target.textContent
        $('.platform').parent().removeClass('active')
        e.target.parentElement.classList.add('active')
        $('#available_flags').html('')
        $.each(AllFlags, function (key, value) {
             if (platform !== 'All') {
                  AllFlags =  AllFlags.filter(flag => flag.platform == platform)
                  return AllFlags
             } else {
                 return AllFlags
             }
        });
        $.each(AllFlags, function (key, value) {
            $('#available_flags').append(`
                    <option class="p-1 h5 border-bottom" value="${value.id}"> ${value.name} : ${value.platform}</option>
                `)
            });
    })
    // ============ list view functions ================
    let product_id = $('#product_id').val()
    const deleteFlag = (id, flag) => {
        $.get(`/products/optimization/flags/delete/${id}/${flag}`)
            .then(res => {
                if (res.success === 200) {
                    $('#successAlert').html(`${res.message}`).fadeIn().fadeOut(4000)
                }
            })
    }
    const saveFlag = (id, flag) => {
        dataToSend = {
            flag: flag,
        }
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="csrf-token"]').val(),
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                type: "POST",
                dataType: 'JSON',
                url: `/products/optimization/flags/save/${id}`,
                data: {
                    flag
                },
            })
            .then(res => {
                if (res.success === 200) {
                    $('#successAlert').html(`${res.message}`).fadeIn().fadeOut(4000)
                }
            })
            .catch(error => {
                console.log(error)
            })
    }
    const listActiveFlagsForProduct = (id) => {
        $.get(`/products/optimization/flagsForProduct/${id}`)
            .then(res => {
                $('#active_flags').html('')
                $.each(res.active_flags, function(key, value) {
                    $('#active_flags').append(`
                         <option class="p-1 h5 border-bottom" value='${value.admin_flag_id}' > ${value.name}  : ${value.platform} </option>
                    `)
                });
            })
            .catch(res => console.log("res", res))
    }
    // delete
    $('.delete').click(function(e) {
        let flag = $('#active_flags option:selected').val()
        e.preventDefault()
        deleteFlag(product_id, flag)
        listActiveFlagsForProduct(product_id)
    })
    // save
    $('.save').click(function(e) {
        let flag = $('#available_flags option:selected').val()
        e.preventDefault()
        saveFlag(product_id, flag)
        listActiveFlagsForProduct(product_id)
    })

});
