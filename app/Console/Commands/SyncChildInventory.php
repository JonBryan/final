<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\CompatiblesProductChildTable;
use App\Models\Products;

class SyncChildInventory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sync-child-inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will sync the child product inventory with the parent product.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = CompatiblesProductChildTable::getParentInventory();
        if (is_object($data)) {
            foreach ($data as $d) {
                $child = Products::find($d->primary_product_id);
                $child->inventory_level = $d->inventory_level;
                $child->save();
            }
        }

        return 0;
    }
}
