<?php

namespace App\Services;

class BigCommerceApi
{
    public function getImage()
    {
        /**
         * This is a test function
         */
        die;
        $id = "21345"; // productID
        $imageID = "50541"; // imageID

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bigcommerce.com/stores/". $_ENV['BIG_COMMERCE_STOREID'] ."/v3/catalog/products/". $id ."/images/". $imageID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "content-type: application/json",
                "x-auth-client: ". $_ENV['BIG_COMMERCE_CLIENT'],
                "x-auth-token: ". $_ENV['BIG_COMMERCE_TOKEN']
            ),
        ));

        $response = curl_exec($curl);
        $json = json_decode($response, true);
        print "<pre>";
        print_r($json);
        print "</pre>";

    }

    public function deleteProduct($id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.bigcommerce.com/stores/". $_ENV['BIG_COMMERCE_STOREID'] ."/v3/catalog/products?id:in=". $id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_NOBODY => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => array(
                "x-auth-client: ". $_ENV['BIG_COMMERCE_CLIENT'],
                "x-auth-token: ". $_ENV['BIG_COMMERCE_TOKEN']
            ),
        ));

        curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        return $status;
    }

    public function UpdateAction($clientId = null, $token = null, $url = null, $post = null)
    {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'PUT',
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
                'X-Auth-Client: ' . $clientId . '',
                'X-Auth-Token: ' . $token . '',
                'Accept: application/json',
                'Content-Type: application/json'
            ),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_VERBOSE => 0
        ));
        // Send the request & save response to $resp
        $result = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        return $result;
    }

    public function PostAction($clientId = null, $token = null, $url = null, $post = null){

        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => array(
                'X-Auth-Client: ' . $clientId . '',
                'X-Auth-Token: ' . $token . '',
                'Accept: application/json',
                'Content-Type: application/json'
            ),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_VERBOSE => 0
        ));
        // Send the request & save response to $resp
        $result = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);

        return $result;
    }

    public function GetAction($clientId = null, $token = null, $url = null, $post = null)
    {
        // Get cURL resource
        $curl = curl_init();
        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => array(
                'X-Auth-Client: ' . $clientId . '',
                'X-Auth-Token: ' . $token . '',
                'Accept: application/json',
                'Content-Type: application/json'
            ),
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_VERBOSE => 0
        ));
        // Send the request & save response to $resp
        $result = curl_exec($curl);
        // Close request to clear up some resources
        curl_close($curl);
        return $result;
    }
}
