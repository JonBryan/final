<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SearchLinksCategories;
use App\Models\SearchLinks;
use App\Models\Products;

class SearchLinksController extends Controller
{
    public function mainListAction($id, Request $request)
    {
        $page_title = "Search Links";
        $product = Products::find($id);
        $categories = SearchLinksCategories::All();

        return view('pages.searchlinks.main', [
            'id' => $id,
            'page_title' => $page_title,
            'product' => $product,
            'categories' => $categories,
        ]);
    }

    public function listAction($id, $categoryID, Request $request)
    {
        $page_title = "Search Links";
        $product = Products::find($id);
        $category = SearchLinksCategories::find($categoryID);
        $data = SearchLinks::getCategoryLinks($id, $categoryID);

        return view('pages.searchlinks.list', [
            'id' => $id,
            'categoryID' => $categoryID,
            'page_title' => $page_title,
            'product' => $product,
            'category' => $category,
            'data' => $data,
        ]);
    }

    public function addAction($id, $categoryID, Request $request)
    {
        $page_title = "Search Links";
        $product = Products::find($id);
        $category = SearchLinksCategories::find($categoryID);

        return view('pages.searchlinks.add', [
            'id' => $id,
            'categoryID' => $categoryID,
            'page_title' => $page_title,
            'product' => $product,
            'category' => $category,
        ]);
    }

    public function saveAction(Request $request)
    {
        $user = $request->user();
        $link = new SearchLinks();
        $link->product_id = $request->request->get('id');
        $link->categoryID = $request->request->get('categoryID');
        $link->title = $request->request->get('title');
        $link->link = $request->request->get('link');
        $link->userID = $user->id;
        $link->date_added = new \DateTime();
        $link->date_updated = new \DateTime();
        $link->save();

        return redirect()->route('products.searchlinks.list', [$request->request->get('id'), $request->request->get('categoryID')])->with('message', "The link was added.");
    }

    public function deleteAction($recordID, $id, $categoryID)
    {
        $link = SearchLinks::find($recordID);
        $link->delete();
        return redirect()->route('products.searchlinks.list', [$id, $categoryID])->with('message', "The link was removed.");
    }
}
