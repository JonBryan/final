<?php

namespace App\Http\Controllers\Products;

use App\Models\Flags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Alarms;
use App\Models\Products;

class AlarmController extends Controller
{
    public function editAction($id, Request $request)
    {
        $page_title = "Alarm";
        $product = Products::find($id);
        $data = Alarms::listAlarms($product);

        return view('pages.alarm.edit', [
            'id' => $id,
            'page_title' => $page_title,
            'data' => $data,
            'product' => $product,
        ]);
    }

    public function saveAction(Request $request)
    {
        $id = $request->request->get('id');

        $product = Products::find($id);
        $user = $request->user();
        $query = Alarms::locateAlarms($product);

        $found = 0;
        foreach ($query as $q) {
            $found = 1;
            if(is_object($q)) {
                $alarm = Alarms::find($q->id);
                $alarm->market_check = $request->request->get('market_check');
                $alarm->price_check = $request->request->get('price_check');
                $alarm->seo_check = $request->request->get('seo_check');
                $alarm->orders_check = $request->request->get('orders_check');
                $alarm->velocity_check = $request->request->get('velocity_check');
                $alarm->buy_box_review = $request->request->get('buy_box_review');
                $alarm->competitor_check = $request->request->get('competitor_check');
                $alarm->other = $request->request->get('other');
                $alarm->comments = $request->request->get('comments');
                $alarm->userID = $user->id;
                $alarm->alarm_date = $request->request->get('alarm_date');
                $alarm->date_updated = new \DateTime();
                $alarm->save();
            }
        }

        if ($found == 0) {
            $alarm = new Alarms();
            $alarm->product_id = $product->id;
            $alarm->market_check = $request->request->get('market_check');
            $alarm->price_check = $request->request->get('price_check');
            $alarm->seo_check = $request->request->get('seo_check');
            $alarm->orders_check = $request->request->get('orders_check');
            $alarm->velocity_check = $request->request->get('velocity_check');
            $alarm->buy_box_review = $request->request->get('buy_box_review');
            $alarm->competitor_check = $request->request->get('competitor_check');
            $alarm->other = $request->request->get('other');
            $alarm->comments = $request->request->get('comments');
            $alarm->userID = $user->id;
            $alarm->alarm_date = $request->request->get('alarm_date');
            $alarm->date_added = new \DateTime();
            $alarm->date_updated = new \DateTime();
            $alarm->save();
        }

        return redirect()->route('products.alarm.edit', $id)->with('message', "The alarm was updated.");
    }
}
