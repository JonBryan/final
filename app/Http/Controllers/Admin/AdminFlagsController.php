<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminManageFlags;

class AdminFlagsController extends Controller
{
    public function listAction(Request $request)
    {
        $page_title = "Manage Flags";

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = AdminManageFlags::orderBy('name')->paginate($_ENV['PAGINATE']);

        return view('pages.admin.flags.list', [
            'data' => $data,
            'page_title' => $page_title,
        ]);

    }

    public function newAction(Request $request)
    {
        $page_title = "New Flag";

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        return view('pages.admin.flags.new', [
            'page_title' => $page_title,
        ]);
    }

    public function saveAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        // verify form
        $request->validate([
            'name' => 'required',
            'platform' => 'required'
        ]);

        $flag = new AdminManageFlags();
        $flag->name = $request->request->get('name');
        $flag->platform = $request->request->get('platform');
        $flag->date_added = new \DateTime();
        $flag->date_updated = new \DateTime();
        $flag->save();

        return redirect()->route('admin.flags.list')->with('message', "The flag was created.");
    }

    public function editAction($id, Request $request)
    {
        $page_title = "Edit Flag";
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = AdminManageFlags::find($id);
        return view('pages.admin.flags.edit', [
            'data' => $data,
            'page_title' => $page_title,
        ]);

    }

    public function updateAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $flag = AdminManageFlags::find($request->request->get('id'));
        $flag->name = $request->request->get('name');
        $flag->platform = $request->request->get('platform');
        $flag->date_updated = new \DateTime();
        $flag->save();
        return redirect()->route('admin.flags.list')->with('message', "The flag was updated.");
    }
}
