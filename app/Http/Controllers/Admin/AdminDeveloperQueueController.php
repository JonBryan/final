<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Models\DeveloperQueue;

class AdminDeveloperQueueController extends Controller
{
    public function listAction(Request $request)
    {
        $page_title = "Developer Queue";

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $data = DeveloperQueue::listRecords();

        return view('pages.admin.developerqueue.list', [
            'page_title' => $page_title,
            'data' => $data,
        ]);
    }

    public function editAction($id, Request $request)
    {
        $page_title = "Developer Queue";
        $data = DeveloperQueue::find($id);
        $product = Products::find($data->product_id);

        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        return view('pages.admin.developerqueue.edit', [
            'page_title' => $page_title,
            'data' => $data,
            'id' => $id,
            'product' => $product,
        ]);

    }

    public function updateAction(Request $request)
    {
        $user = $request->user();

        if ($user->hasRole($_ENV['ADMIN_ROLE']) != true) {
            return view('error.access-denied');
        }

        $queue = DeveloperQueue::find($request->request->get('id'));
        if ($request->request->get('bug') != "") {
            $bug = "on";
        } else {
            $bug = "off";
        }
        $queue->bug = $bug;

        if ($request->request->get('new_feature') != "") {
            $new_feature = "on";
        } else {
            $new_feature = "off";
        }
        $queue->new_feature = $new_feature;

        if ($request->request->get('question') != "") {
            $question = "on";
        } else {
            $question = "off";
        }
        $queue->question = $question;

        if ($request->request->get('other') != "") {
            $other = "on";
        } else {
            $other = "off";
        }
        $queue->other = $other;

        $queue->comments = $request->request->get('comments');
        $queue->status = $request->request->get('status');

        $queue->date_updated = new \DateTime();
        $queue->save();
        return redirect()->route('admin.developerqueue.list')->with('message', 'The queue was updated');
    }
}
