<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoxCategories extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'box_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'category'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the boxSubCategories for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function boxSubCategories()
    {
        return $this->hasMany('App\Models\BoxSubCategory','box_category_id','id');
    }

    /**
     * Get the loads for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function loads()
    {
        return $this->hasMany('App\Models\Load','box_category_id','id');
    }



}
