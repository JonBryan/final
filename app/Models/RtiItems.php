<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RtiItems extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rti_items';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'load_id',
                  'rti_box_id',
                  'product_id',
                  'mpn',
                  'brand',
                  'sku',
                  'bin_picking_number',
                  'inventory_level',
                  'inventory_warning_level',
                  'upc',
                  'asin',
                  'price',
                  'productCondition',
                  'google_shopping_on',
                  'google_shopping_exclude',
                  'user_id',
                  'on_market',
                  'date_created',
                  'product_status'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the Load for this model.
     *
     * @return App\Models\Load
     */
    public function Load()
    {
        return $this->belongsTo('App\Models\Load','load_id','id');
    }

    /**
     * Get the RtiBox for this model.
     *
     * @return App\Models\RtiBox
     */
    public function RtiBox()
    {
        return $this->belongsTo('App\Models\RtiBox','rti_box_id','id');
    }

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * Get the User for this model.
     *
     * @return App\Models\User
     */
    public function User()
    {
        return $this->belongsTo('App\Models\User','user_id','id');
    }



}
