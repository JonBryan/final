<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbaySuggestBuyReportsSingleRough extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ebay_suggest_buy_reports_single_rough';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'title',
                  'sku',
                  'brand',
                  'mpn',
                  'result_mpn',
                  'item_id',
                  'seller',
                  'seller_feedback_score',
                  'seller_feedback_percentage',
                  'seller_location',
                  'price',
                  'shipping_price',
                  'item_condition',
                  'listing_type',
                  'selling_state',
                  'url',
                  'quantity_sold',
                  'is_match',
                  'inventory_level',
                  'sold_qty_1',
                  'sold_qty_2',
                  'sold_qty_6',
                  'sold_qty_12'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the item for this model.
     *
     * @return App\Models\Item
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Item','item_id');
    }



}
