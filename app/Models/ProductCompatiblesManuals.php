<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCompatiblesManuals extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_compatibles_manuals';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'compatibles_id',
        'link',
        'note',
        'manual_id'
    ];

    public static function listManuals($product, $compatiblesID)
    {
        $result = ProductCompatiblesManuals::from('products_compatibles_manuals as m')
            ->select(
                'm.id',
                'm.product_id',
                'm.compatibles_id',
                't.manual_type',
                'm.link',
                'm.note'
            )
            ->leftJoin('compatibles_manual_types as t', function($leftJoin)
            {
                $leftJoin->on('t.id', '=', 'm.manual_id');
            })
            ->where('m.product_id', $product->id)
            ->where('m.compatibles_id', $compatiblesID)
            ->get()
        ;
        return $result;
    }

    public static function deleteAllManuals($compatableID)
    {
        $array = array($compatableID);
        ProductCompatiblesManuals::from('products_compatibles_manuals')->whereIn('compatibles_id', $array)->delete();
        return true;
    }
}
