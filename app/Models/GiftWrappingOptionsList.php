<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiftWrappingOptionsList extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gift_wrapping_options_list';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'gift_wrapping_options_list'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * @param $product
     * @param $list
     * @return bool
     */
    public static function insertGiftWrappingOptionList($product, $list)
    {
        for($i = 0; $i < count($list); $i++){
            $gift = new GiftWrappingOptionsList();
            $gift->product_id = $product->id;
            $gift->gift_wrapping_options_list = $list[$i];
            $gift->save();
        }
        return true;
    }

}
