<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'is_default',
                  'sort_order',
                  'category_id',
                  'name',
                  'description',
                  'page_title',
                  'parent_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the category for this model.
     *
     * @return App\Models\Category
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category','category_id');
    }

    /**
     * Get the parent for this model.
     *
     * @return App\Models\Parent
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Parent','parent_id');
    }

    public static function getDefaultCategory()
    {
        $data = ProductCategories::from('product_categories as p')
            ->select(
                'p.id',
                'p.is_default',
                'p.sort_order',
                'p.category_id',
                'p.name',
                'p.description',
                'p.page_title',
                'p.parent_id'
            )
            ->where('p.is_default', '=', '1')
            ->take(1)
            ->get();
        return $data;
    }

    public static function getBrandCategory($brandName)
    {
        $data = ProductCategories::from('product_categories as p')
            ->select(
                'p.id',
                'p.is_default',
                'p.sort_order',
                'p.category_id',
                'p.name',
                'p.description',
                'p.page_title',
                'p.parent_id'
            )
            ->where('p.name', '=', $brandName)
            ->take(1)
            ->get();
        return $data;
    }

}
