<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'store_id',
                  'order_id',
                  'customer_id',
                  'date_created',
                  'date_modified',
                  'date_shipped',
                  'status_id',
                  'status',
                  'subtotal_ex_tax',
                  'subtotal_inc_tax',
                  'subtotal_tax',
                  'base_shipping_cost',
                  'shipping_cost_ex_tax',
                  'shipping_cost_inc_tax',
                  'shipping_cost_tax',
                  'shipping_cost_tax_class_id',
                  'base_handling_cost',
                  'handling_cost_ex_tax',
                  'handling_cost_inc_tax',
                  'handling_cost_tax',
                  'handling_cost_tax_class_id',
                  'base_wrapping_cost',
                  'wrapping_cost_ex_tax',
                  'wrapping_cost_inc_tax',
                  'wrapping_cost_tax',
                  'wrapping_cost_tax_class_id',
                  'total_ex_tax',
                  'total_inc_tax',
                  'total_tax',
                  'items_total',
                  'items_shipped',
                  'payment_method',
                  'payment_provider_id',
                  'payment_status',
                  'refunded_amount',
                  'order_is_digital',
                  'store_credit_amount',
                  'gift_certificate_amount',
                  'ip_address',
                  'geoip_country',
                  'geoip_country_iso2',
                  'currency_id',
                  'currency_code',
                  'currency_exchange_rate',
                  'default_currency_id',
                  'default_currency_code',
                  'staff_notes',
                  'customer_message',
                  'discount_amount',
                  'coupon_discount',
                  'shipping_address_count',
                  'is_deleted',
                  'ebay_order_id',
                  'first_name',
                  'last_name',
                  'company',
                  'street1',
                  'street2',
                  'city',
                  'state',
                  'zip',
                  'country',
                  'country_iso2',
                  'phone',
                  'email',
                  'is_email_opt_in',
                  'credit_card_type',
                  'order_source',
                  'external_source'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the store for this model.
     *
     * @return App\Models\Store
     */
    public function store()
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }

    /**
     * Get the order for this model.
     *
     * @return App\Models\Order
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order','order_id');
    }

    /**
     * Get the customer for this model.
     *
     * @return App\Models\Customer
     */
    public function customer()
    {
        return $this->belongsTo('App\Models\Customer','customer_id');
    }

    /**
     * Get the status for this model.
     *
     * @return App\Models\Status
     */
    public function status()
    {
        return $this->belongsTo('App\Models\Status','status_id');
    }

    /**
     * Get the shippingCostTaxClass for this model.
     *
     * @return App\Models\ShippingCostTaxClass
     */
    public function shippingCostTaxClass()
    {
        return $this->belongsTo('App\Models\ShippingCostTaxClass','shipping_cost_tax_class_id');
    }

    /**
     * Get the handlingCostTaxClass for this model.
     *
     * @return App\Models\HandlingCostTaxClass
     */
    public function handlingCostTaxClass()
    {
        return $this->belongsTo('App\Models\HandlingCostTaxClass','handling_cost_tax_class_id');
    }

    /**
     * Get the wrappingCostTaxClass for this model.
     *
     * @return App\Models\WrappingCostTaxClass
     */
    public function wrappingCostTaxClass()
    {
        return $this->belongsTo('App\Models\WrappingCostTaxClass','wrapping_cost_tax_class_id');
    }

    /**
     * Get the paymentProvider for this model.
     *
     * @return App\Models\PaymentProvider
     */
    public function paymentProvider()
    {
        return $this->belongsTo('App\Models\PaymentProvider','payment_provider_id');
    }

    /**
     * Get the currency for this model.
     *
     * @return App\Models\Currency
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency','currency_id');
    }

    /**
     * Get the defaultCurrency for this model.
     *
     * @return App\Models\DefaultCurrency
     */
    public function defaultCurrency()
    {
        return $this->belongsTo('App\Models\DefaultCurrency','default_currency_id');
    }

    /**
     * Get the ebayOrder for this model.
     *
     * @return App\Models\EbayOrder
     */
    public function ebayOrder()
    {
        return $this->belongsTo('App\Models\EbayOrder','ebay_order_id');
    }

    /**
     * Get the productOrders for this model.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function productOrders()
    {
        return $this->hasMany('App\Models\ProductOrder','orders_id','id');
    }



}
