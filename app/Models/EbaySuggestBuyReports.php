<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EbaySuggestBuyReports extends Model
{
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ebay_suggest_buy_reports';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'root_sku',
                  'title',
                  'sku',
                  'brand',
                  'mpn',
                  'result_mpn',
                  'item_id',
                  'seller',
                  'seller_feedback_score',
                  'seller_feedback_percentage',
                  'seller_location',
                  'price',
                  'shipping_price',
                  'item_condition',
                  'listing_type',
                  'selling_state',
                  'url',
                  'date_timestamp',
                  'date_time',
                  'quantity_sold',
                  'is_match'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the item for this model.
     *
     * @return App\Models\Item
     */
    public function item()
    {
        return $this->belongsTo('App\Models\Item','item_id');
    }



}
