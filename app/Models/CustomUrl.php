<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomUrl extends Model
{

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_url';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'product_id',
                  'url',
                  'is_customized'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the Product for this model.
     *
     * @return App\Models\Product
     */
    public function Product()
    {
        return $this->belongsTo('App\Models\Product','product_id','id');
    }

    /**
     * @param $product
     * @param $urls
     * @return bool
     */
    public static function insertCustomUrl($product, $urls)
    {
        $custom = new CustomUrl();
        $custom->product_id = $product->id;
        $custom->url = $urls['url'];
        $custom->is_customized = $urls['is_customized'];
        $custom->save();
        return true;
    }

    public static function getProductUrl($product)
    {
        $data = CustomUrl::from('custom_url as c')
            ->select(
                'c.url'
            )
            ->where('c.product_id', '=', $product->id)
            ->get()
        ;
        return $data;
    }

}
